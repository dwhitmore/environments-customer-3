#!/bin/bash
set -e

HE_DIR=$1
HOST_NAME=$2

echo ""
echo "********** REDCARD SH : redcard_post_install.sh start on ${HOST_NAME} **********"

        echo "Copying config xml to KARAF_HOME/xml folder"
        cp ./Paramount_RedCard_Inbound_Config.xml $HE_DIR/xml/
    
echo "********** REDCARD SH : redcard_post_install.sh end on ${HOST_NAME} **********"
echo ""
