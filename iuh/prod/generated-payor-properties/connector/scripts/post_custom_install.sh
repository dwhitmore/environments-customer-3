#!/bin/bash

iuhLog=/home/iuh_cprime/staging/post_custom_install.log
echo "Start of script post_custom_install.sh "`date` >> $iuhLog
echo "hostname is: "$HOSTNAME >> $iuhLog
custom_package_location=$5
cd /home/iuh_cprime/job
echo "pwd:" `pwd` >> $iuhLog

#DEV env
#==========================================================================================================
if [ "$HOSTNAME" = "IUH-CON-DV01" ]; then
   echo "Entered into DEV">> $iuhLog
   if [ -d /home/iuh_cprime/job/iuh-208/custom/customFileTransfers ]; then
      echo "/home/iuh_cprime/job/iuh-208/custom/customFileTransfers already exists">> $iuhLog
   else
      echo "Creating dir: ~/job/iuh-208/custom/customFileTransfers">> $iuhLog
      mkdir -p /home/iuh_cprime/job/iuh-208/custom/customFileTransfers
      echo "Adjust permissions on dir">> $iuhLog
      chmod 775 ~/job/iuh-208/custom/customFileTransfers
      echo "echo dir structure">> $iuhLog
      ls -ltr ~/job/iuh-208/custom>> $iuhLog
   fi
   
   if [ -f /home/iuh_cprime/job/iuh-208/custom/customFileTransfers/iuh_dev_custom_file_xfer.sh ]; then
      echo "/home/iuh_cprime/job/iuh-208/custom/customFileTransfers/iuh_dev_custom_file_xfer.sh already exists">>$iuhLog
   else
##      cp ${custom_package_location}/additional-package/iuh_env_custom_file_xfer.zip /home/iuh_cprime/job/iuh-208/custom/customFileTransfers
      cp /home/iuh_cprime/job/iuh_env_custom_file_xfer.zip /home/iuh_cprime/job/iuh-208/custom/customFileTransfers
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with cp command iuh_env_custom_file_xfer.zip to customFileTransfers directory">>$iuhLog 
      fi

      echo "Unzip deployment scripts in dir /home/iuh_cprime/job/iuh-208/custom/customFileTransfers">>$iuhLog 
      cd ./iuh-208/custom/customFileTransfers
      echo "pwd:"`pwd`>>$iuhLog
      unzip -o iuh_env_custom_file_xfer.zip
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with unzip command in dir /home/iuh_cprime/job/iuh-208/custom/customFileTransfers">>$iuhLog 
      fi
   fi
fi


#SANDBOX env
#==========================================================================================================
if [ "$HOSTNAME" = "IUH-CON-SB01" ]; then
   echo "Entered into SANDBOX">> $iuhLog
   if [ -d /home/iuh_cprime/job/iuh-405/custom/customFileTransfers ]; then
      echo "/home/iuh_cprime/job/iuh-405/custom/customFileTransfers already exists">> $iuhLog
   else
      echo "Creating dir: ~/job/iuh-405/custom/customFileTransfers">> $iuhLog
      mkdir -p /home/iuh_cprime/job/iuh-405/custom/customFileTransfers
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with create dir structure /home/iuh_cprime/custom/customFileTransfers ">>$iuhLog 
      fi
      echo "Adjust permissions on dir">> $iuhLog
      chmod 775 /home/iuh_cprime/job/iuh-405/custom/customFileTransfers
      echo "echo dir structure">> $iuhLog
      ls -ltr /home/iuh_cprime/job/iuh-405/custom>> $iuhLog
   fi
   
   if [ -f /home/iuh_cprime/job/iuh-405/custom/customFileTransfers/iuh_uat_custom_file_xfer.sh ]; then
      echo "/home/iuh_cprime/job/iuh-405/custom/customFileTransfers/iuh_uat_custom_file_xfer.sh already exists">>$iuhLog
   else
      cp ${custom_package_location}/additional-package/iuh_env_custom_file_xfer.zip /home/iuh_cprime/job/iuh-405/custom/customFileTransfers
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with cp command iuh_env_custom_file_xfer.zip to customFileTransfers directory">>$iuhLog 
      fi

      echo "Unzip deployment scripts in dir /home/iuh_cprime/job/iuh-405/custom/customFileTransfers">>$iuhLog 
      cd ./iuh-405/custom/customFileTransfers
      echo "pwd:"`pwd`>>$iuhLog
      unzip -o iuh_env_custom_file_xfer.zip
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with unzip command in dir /home/iuh_cprime/job/iuh-405/custom/customFileTransfers">>$iuhLog 
      fi
   fi
fi

#CONFIG env
#==========================================================================================================
if [ "$HOSTNAME" = "iuh-con-cf01" ]; then
   echo "Entered into CONFIG">> $iuhLog
   if [ -d /home/iuh_cprime/job/iuh-305/custom/customFileTransfers ]; then
      echo "/home/iuh_cprime/job/iuh-305/custom/customFileTransfers already exists">> $iuhLog
   else
      echo "Creating dir: ~/job/iuh-305/custom/customFileTransfers">> $iuhLog
      mkdir -p /home/iuh_cprime/job/iuh-305/custom/customFileTransfers
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with create dir structure /home/iuh_cprime/custom/customFileTransfers ">>$iuhLog 
      fi
      echo "Adjust permissions on dir">> $iuhLog
      chmod 775 /home/iuh_cprime/job/iuh-305/custom/customFileTransfers
      echo "echo dir structure">> $iuhLog
      ls -ltr /home/iuh_cprime/job/iuh-305/custom>> $iuhLog
   fi
   
   if [ -f /home/iuh_cprime/job/iuh-305/custom/customFileTransfers/iuh_uat_custom_file_xfer.sh ]; then
      echo "/home/iuh_cprime/job/iuh-305/custom/customFileTransfers/iuh_uat_custom_file_xfer.sh already exists">>$iuhLog
   else
      cp ${custom_package_location}/additional-package/iuh_env_custom_file_xfer.zip /home/iuh_cprime/job/iuh-305/custom/customFileTransfers
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with cp command iuh_env_custom_file_xfer.zip to customFileTransfers directory">>$iuhLog 
      fi

      echo "Unzip deployment scripts in dir /home/iuh_cprime/job/iuh-305/custom/customFileTransfers">>$iuhLog 
      cd ./iuh-305/custom/customFileTransfers
      echo "pwd:"`pwd`>>$iuhLog
      unzip -o iuh_env_custom_file_xfer.zip
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with unzip command in dir /home/iuh_cprime/job/iuh-305/custom/customFileTransfers">>$iuhLog 
      fi
   fi
fi


#UAT env
#==========================================================================================================
if [ "$HOSTNAME" = "IUH-CON-UT01" ]; then
   echo "Entered into UAT">> $iuhLog
   if [ -d /home/iuh_cprime/job/iuh-205/custom/customFileTransfers ]; then
      echo "/home/iuh_cprime/job/iuh-205/custom/customFileTransfers already exists">> $iuhLog
   else
      echo "Creating dir: ~/job/iuh-205/custom/customFileTransfers">> $iuhLog
      mkdir -p /home/iuh_cprime/job/iuh-205/custom/customFileTransfers
      echo "Adjust permissions on dir">> $iuhLog
      chmod 775 ~/job/iuh-205/custom/customFileTransfers
      echo "echo dir structure">> $iuhLog
      ls -ltr ~/job/iuh-205/custom>> $iuhLog
   fi
   
   if [ -f /home/iuh_cprime/job/iuh-205/custom/customFileTransfers/iuh_uat_custom_file_xfer.sh ]; then
      echo "/home/iuh_cprime/job/iuh-205/custom/customFileTransfers/iuh_uat_custom_file_xfer.sh already exists">>$iuhLog
   else
      cp ${custom_package_location}/additional-package/iuh_env_custom_file_xfer.zip /home/iuh_cprime/job/iuh-205/custom/customFileTransfers
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with cp command iuh_env_custom_file_xfer.zip to customFileTransfers directory">>$iuhLog 
      fi

      echo "Unzip deployment scripts in dir /home/iuh_cprime/job/iuh-205/custom/customFileTransfers">>$iuhLog 
      cd ./iuh-205/custom/customFileTransfers
      echo "pwd:"`pwd`>>$iuhLog
      unzip -o iuh_env_custom_file_xfer.zip
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with unzip command in dir /home/iuh_cprime/job/iuh-205/custom/customFileTransfers">>$iuhLog 
      fi
   fi
fi

#PPROD env
#==========================================================================================================
if [ "$HOSTNAME" = "IUH-CON-PP01" ]; then
   echo "Entered into PREPROD">> $iuhLog
   if [ -d /home/iuh_cprime/job/iuh-202/custom/customFileTransfers ]; then
      echo "/home/iuh_cprime/job/iuh-202/custom/customFileTransfers already exists">> $iuhLog
   else
      echo "Creating dir: ~/job/iuh-202/custom/customFileTransfers">> $iuhLog
      mkdir -p /home/iuh_cprime/job/iuh-202/custom/customFileTransfers
      echo "Adjust permissions on dir">> $iuhLog
      chmod 775 ~/job/iuh-202/custom/customFileTransfers
      echo "echo dir structure">> $iuhLog
      ls -ltr ~/job/iuh-202/custom>> $iuhLog
   fi
   
   if [ -f /home/iuh_cprime/job/iuh-202/custom/customFileTransfers/iuh_pprod_custom_file_xfer.sh ]; then
      echo "/home/iuh_cprime/job/iuh-202/custom/customFileTransfers/iuh_pprod_custom_file_xfer.sh already exists">>$iuhLog
   else
      cp ${custom_package_location}/additional-package/iuh_env_custom_file_xfer.zip /home/iuh_cprime/job/iuh-202/custom/customFileTransfers
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with cp command iuh_env_custom_file_xfer.zip to customFileTranfers directory">>$iuhLog 
      fi

      echo "Unzip deployment scripts in dir /home/iuh_cprime/job/iuh-202/custom/customFileTransfers">>$iuhLog 
      cd ./iuh-202/custom/customFileTransfers
      echo "pwd:"`pwd`>>$iuhLog
      unzip -o iuh_env_custom_file_xfer.zip
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with unzip command in dir /home/iuh_cprime/job/iuh-202/custom/customFileTransfers">>$iuhLog 
      fi
   fi
fi

#PROD env
#==========================================================================================================
if [ "$HOSTNAME" = "iuh-con-pr01" ]; then
   echo "Entered into PROD">> $iuhLog
   if [ -d /home/iuh_cprime/job/iuh-102/custom/customFileTransfers ]; then
      echo "/home/iuh_cprime/job/iuh-102/custom/customFileTransfers already exists">> $iuhLog
   else
      echo "Creating dir: ~/job/iuh-102/custom/customFileTransfers">> $iuhLog
      mkdir -p /home/iuh_cprime/job/iuh-102/custom/customFileTransfers
      echo "Adjust permissions on dir">> $iuhLog
      chmod 775 ~/job/iuh-102/custom/customFileTransfers
      echo "echo dir structure">> $iuhLog
      ls -ltr ~/job/iuh-102/custom>> $iuhLog
   fi
   
   if [ -f /home/iuh_cprime/job/iuh-102/custom/customFileTransfers/iuh_prod_custom_file_xfer.sh ]; then
      echo "/home/iuh_cprime/job/iuh-102/custom/customFileTransfers/iuh_prod_custom_file_xfer.sh already exists">>$iuhLog
   else
      cp ${custom_package_location}/additional-package/iuh_env_custom_file_xfer.zip /home/iuh_cprime/job/iuh-102/custom/customFileTransfers
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with cp command iuh_env_custom_file_xfer.zip to customFileTransfers directory">>$iuhLog 
      fi

      echo "Unzip deployment scripts in dir /home/iuh_cprime/job/iuh-102/custom/customFileTransfers">>$iuhLog 
      cd ./iuh-102/custom/customFileTransfers
      echo "pwd:"`pwd`>>$iuhLog
      unzip -o iuh_env_custom_file_xfer.zip
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with unzip command in dir /home/iuh_cprime/job/iuh-102/custom/customFileTransfers">>$iuhLog 
      fi
   fi
fi


#PROD env
#==========================================================================================================
if [ "$HOSTNAME" = "iuh-con-pr02" ]; then
   echo "Entered into PROD">> $iuhLog
   if [ -d /home/iuh_cprime/job/iuh-102/custom/customFileTransfers ]; then
      echo "/home/iuh_cprime/job/iuh-102/custom/customFileTransfers already exists">> $iuhLog
   else
      echo "Creating dir: ~/job/iuh-102/custom/customFileTransfers">> $iuhLog
      mkdir -p /home/iuh_cprime/job/iuh-102/custom/customFileTransfers
      echo "Adjust permissions on dir">> $iuhLog
      chmod 775 ~/job/iuh-102/custom/customFileTransfers
      echo "echo dir structure">> $iuhLog
      ls -ltr ~/job/iuh-102/custom>> $iuhLog
   fi
   
   if [ -f /home/iuh_cprime/job/iuh-102/custom/customFileTransfers/iuh_prod_custom_file_xfer.sh ]; then
      echo "/home/iuh_cprime/job/iuh-102/custom/customFileTransfers/iuh_prod_custom_file_xfer.sh already exists">>$iuhLog
   else
      cp ${custom_package_location}/additional-package/iuh_env_custom_file_xfer.zip /home/iuh_cprime/job/iuh-102/custom/customFileTransfers
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with cp command iuh_env_custom_file_xfer.zip to customFileTransfers directory">>$iuhLog 
      fi

      echo "Unzip deployment scripts in dir /home/iuh_cprime/job/iuh-102/custom/customFileTransfers">>$iuhLog 
      cd ./iuh-102/custom/customFileTransfers
      echo "pwd:"`pwd`>>$iuhLog
      unzip -o iuh_env_custom_file_xfer.zip
      myRtn=`echo $?`
      if [ ${myRtn} -ne 0 ]; then
        echo "Issue with unzip command in dir /home/iuh_cprime/job/iuh-102/custom/customFileTransfers">>$iuhLog 
      fi
   fi
fi

echo "Completed  script post_custom_install.sh "`date` >> $iuhLog
